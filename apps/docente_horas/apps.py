from django.apps import AppConfig


class DocenteHorasConfig(AppConfig):
    name = 'docente_horas'
