from django.apps import AppConfig


class DocenteAreaConfig(AppConfig):
    name = 'docente_area'
