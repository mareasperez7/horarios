from django.apps import AppConfig


class AreaConfig(AppConfig):
    name = 'area'
    verbose_name = 'Areas'
