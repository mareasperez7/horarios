from django.apps import AppConfig


class PlanDeEstudioConfig(AppConfig):
    name = 'plan_de_estudio'
